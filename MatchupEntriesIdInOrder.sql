CREATE DEFINER = `root`@`localhost` PROCEDURE `MatchupEntriesIDInOrder`()
BEGIN
	SELECT *
	FROM matchupentries
	WHERE Id < 100
	ORDER BY Id
;
END