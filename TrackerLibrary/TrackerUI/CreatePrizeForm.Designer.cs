﻿namespace TrackerUI
{
    partial class CreatePrizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlaceNumberText = new System.Windows.Forms.TextBox();
            this.PlaceNumberLabel = new System.Windows.Forms.Label();
            this.PlaceNameTextBox = new System.Windows.Forms.TextBox();
            this.CellphoneLabel = new System.Windows.Forms.Label();
            this.PlaceNameLabel = new System.Windows.Forms.Label();
            this.PricePercentageTextBox = new System.Windows.Forms.TextBox();
            this.PrizeAmountTextBox = new System.Windows.Forms.TextBox();
            this.PrizeAmountLabel = new System.Windows.Forms.Label();
            this.CreatePrizeLabel = new System.Windows.Forms.Label();
            this.ORLabel = new System.Windows.Forms.Label();
            this.CreatePrizeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PlaceNumberText
            // 
            this.PlaceNumberText.AccessibleName = "placeNumberText";
            this.PlaceNumberText.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNumberText.Location = new System.Drawing.Point(277, 124);
            this.PlaceNumberText.Name = "PlaceNumberText";
            this.PlaceNumberText.Size = new System.Drawing.Size(160, 33);
            this.PlaceNumberText.TabIndex = 32;
            // 
            // PlaceNumberLabel
            // 
            this.PlaceNumberLabel.AccessibleName = "placeNumberLabel";
            this.PlaceNumberLabel.AutoSize = true;
            this.PlaceNumberLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNumberLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PlaceNumberLabel.Location = new System.Drawing.Point(87, 127);
            this.PlaceNumberLabel.Name = "PlaceNumberLabel";
            this.PlaceNumberLabel.Size = new System.Drawing.Size(144, 30);
            this.PlaceNumberLabel.TabIndex = 33;
            this.PlaceNumberLabel.Text = "Place Number";
            // 
            // PlaceNameTextBox
            // 
            this.PlaceNameTextBox.AccessibleName = "placeNameText";
            this.PlaceNameTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNameTextBox.Location = new System.Drawing.Point(277, 163);
            this.PlaceNameTextBox.Name = "PlaceNameTextBox";
            this.PlaceNameTextBox.Size = new System.Drawing.Size(160, 33);
            this.PlaceNameTextBox.TabIndex = 34;
            // 
            // CellphoneLabel
            // 
            this.CellphoneLabel.AccessibleName = "PrizePercentageLabel";
            this.CellphoneLabel.AutoSize = true;
            this.CellphoneLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CellphoneLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CellphoneLabel.Location = new System.Drawing.Point(92, 333);
            this.CellphoneLabel.Name = "CellphoneLabel";
            this.CellphoneLabel.Size = new System.Drawing.Size(167, 30);
            this.CellphoneLabel.TabIndex = 39;
            this.CellphoneLabel.Text = "Prize Percentage";
            // 
            // PlaceNameLabel
            // 
            this.PlaceNameLabel.AccessibleName = "placeNameLabel";
            this.PlaceNameLabel.AutoSize = true;
            this.PlaceNameLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PlaceNameLabel.Location = new System.Drawing.Point(87, 166);
            this.PlaceNameLabel.Name = "PlaceNameLabel";
            this.PlaceNameLabel.Size = new System.Drawing.Size(124, 30);
            this.PlaceNameLabel.TabIndex = 35;
            this.PlaceNameLabel.Text = "Place Name";
            // 
            // PricePercentageTextBox
            // 
            this.PricePercentageTextBox.AccessibleName = "firstNameText";
            this.PricePercentageTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PricePercentageTextBox.Location = new System.Drawing.Point(277, 330);
            this.PricePercentageTextBox.Name = "PricePercentageTextBox";
            this.PricePercentageTextBox.Size = new System.Drawing.Size(160, 33);
            this.PricePercentageTextBox.TabIndex = 38;
            // 
            // PrizeAmountTextBox
            // 
            this.PrizeAmountTextBox.AccessibleName = "prizeAmountText";
            this.PrizeAmountTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrizeAmountTextBox.Location = new System.Drawing.Point(277, 203);
            this.PrizeAmountTextBox.Name = "PrizeAmountTextBox";
            this.PrizeAmountTextBox.Size = new System.Drawing.Size(160, 33);
            this.PrizeAmountTextBox.TabIndex = 36;
            // 
            // PrizeAmountLabel
            // 
            this.PrizeAmountLabel.AccessibleName = "prizeAmountLabel";
            this.PrizeAmountLabel.AutoSize = true;
            this.PrizeAmountLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrizeAmountLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PrizeAmountLabel.Location = new System.Drawing.Point(87, 206);
            this.PrizeAmountLabel.Name = "PrizeAmountLabel";
            this.PrizeAmountLabel.Size = new System.Drawing.Size(139, 30);
            this.PrizeAmountLabel.TabIndex = 37;
            this.PrizeAmountLabel.Text = "Prize Amount";
            // 
            // CreatePrizeLabel
            // 
            this.CreatePrizeLabel.AccessibleName = "CreatePrizeLabel";
            this.CreatePrizeLabel.AutoSize = true;
            this.CreatePrizeLabel.Font = new System.Drawing.Font("Segoe UI Light", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatePrizeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreatePrizeLabel.Location = new System.Drawing.Point(37, 27);
            this.CreatePrizeLabel.Name = "CreatePrizeLabel";
            this.CreatePrizeLabel.Size = new System.Drawing.Size(208, 50);
            this.CreatePrizeLabel.TabIndex = 40;
            this.CreatePrizeLabel.Text = "Create Price";
            // 
            // ORLabel
            // 
            this.ORLabel.AccessibleName = "ORLabel";
            this.ORLabel.AutoSize = true;
            this.ORLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ORLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.ORLabel.Location = new System.Drawing.Point(230, 277);
            this.ORLabel.Name = "ORLabel";
            this.ORLabel.Size = new System.Drawing.Size(58, 30);
            this.ORLabel.TabIndex = 41;
            this.ORLabel.Text = "-OR-";
            // 
            // CreatePrizeButton
            // 
            this.CreatePrizeButton.AccessibleName = "createPrizeButton";
            this.CreatePrizeButton.BackColor = System.Drawing.Color.White;
            this.CreatePrizeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.CreatePrizeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.CreatePrizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreatePrizeButton.Font = new System.Drawing.Font("Segoe UI Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatePrizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreatePrizeButton.Location = new System.Drawing.Point(199, 409);
            this.CreatePrizeButton.Name = "CreatePrizeButton";
            this.CreatePrizeButton.Size = new System.Drawing.Size(205, 64);
            this.CreatePrizeButton.TabIndex = 42;
            this.CreatePrizeButton.Text = "Create Prize";
            this.CreatePrizeButton.UseVisualStyleBackColor = false;
            // 
            // CreatePrizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(543, 547);
            this.Controls.Add(this.CreatePrizeButton);
            this.Controls.Add(this.ORLabel);
            this.Controls.Add(this.CreatePrizeLabel);
            this.Controls.Add(this.PlaceNumberText);
            this.Controls.Add(this.PlaceNumberLabel);
            this.Controls.Add(this.PlaceNameTextBox);
            this.Controls.Add(this.CellphoneLabel);
            this.Controls.Add(this.PlaceNameLabel);
            this.Controls.Add(this.PricePercentageTextBox);
            this.Controls.Add(this.PrizeAmountTextBox);
            this.Controls.Add(this.PrizeAmountLabel);
            this.Name = "CreatePrizeForm";
            this.Text = "CreatePrizeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PlaceNumberText;
        private System.Windows.Forms.Label PlaceNumberLabel;
        private System.Windows.Forms.TextBox PlaceNameTextBox;
        private System.Windows.Forms.Label CellphoneLabel;
        private System.Windows.Forms.Label PlaceNameLabel;
        private System.Windows.Forms.TextBox PricePercentageTextBox;
        private System.Windows.Forms.TextBox PrizeAmountTextBox;
        private System.Windows.Forms.Label PrizeAmountLabel;
        private System.Windows.Forms.Label CreatePrizeLabel;
        private System.Windows.Forms.Label ORLabel;
        private System.Windows.Forms.Button CreatePrizeButton;
    }
}