﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    public class MatchupEntryModel
    {
        // Represents one team in the matchup
        public TeamModel TeamCompeting { get; set; }
        //Score for particular team
        public double Score { get; set; }
        //Represent matchup team came from as winner
        public MatchupModel ParentMatchup { get; set; }
    }

}

