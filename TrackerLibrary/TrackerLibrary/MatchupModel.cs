﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    public class MatchupModel
    {
        //lists of matchup entry 
        public List<MatchupEntryModel> Entrie { get; set; } = new List<MatchupEntryModel>();
        //Winner teams
        public TeamModel Winner { get; set; }
        //Which rounds
        public int MatchupRound { get; set; }
    }
}
