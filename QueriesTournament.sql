
DELETE FROM matchupentries;
DELETE FROM matchups;
DELETE FROM TournamentPrices;
DELETE FROM TournamentEntries;
DELETE FROM Tournament;
DELETE FROM TeamMembers;
DELETE FROM Prizes;
DELETE FROM People;
DELETE FROM Teams;




INSERT INTO Teams
	(Id, TeamName) VALUES
    (2, 'Kinail team'),
    (5, 'Kuro team'),
    (4, 'Alexander team'),
    (3, 'Samantha team')
;

INSERT INTO People
	(Id, FirstName, LastName, EmailAdress, Cellphone) VALUES
    (1, 'Alexander', 'Boulle', 'alex.boulle@gsomething.com', '592-5831'),
	(2, 'Kinail', 'FatCat', 'kinail.fatcat@gsomething.com', '892-6335'),
	(3, 'Kuro', 'BabyCat', 'kuro.babycat@gsomething.com', 'none'),
    (4, 'Samantha', 'Dup', 'sam.dup@gsomething.com', '168-4132')
;

INSERT INTO Prizes
	(id, PlaceNumber, PlaceName, PrizeAmount, PrizePercentage) VALUES
    (2336, 1, 'First', 1000.00, .1),
    (41, 2, 'Second', 500.00, .05),
    (562, 3, 'Third', 200.00, .02),
	(2337, 1, 'First2', 2000.00, .1)
;

INSERT INTO TeamMembers
	(Id, TeamId, PersonId) VALUES
    (4, 5, 1),
    (1, 4, 2),
    (3, 2, 4),
    (2, 3, 3)
    ; 

INSERT INTO Tournament
	(Id, TournamentName, EntryFee) VALUES
    (1, 'SupaTournament', 156.50),
    (2, 'THETournament', 1000.00),
    (3, 'ScoutChampionship', 545.89),
    (4, 'AnotherTournament', 659.90)
;

INSERT INTO TournamentEntries
	(Id, TournamentTEId, TeamTEId) VALUES
    (454, 1, 2),
    (983, 2, 5),
    (181, 3, 3),
    (321, 4, 4)
;
INSERT INTO tournamentprices
	(id, TournamentPId, PriceId) VALUES
    (654, 1, 2336),
    (268, 2, 41),
    (698, 3, 562),
    (354, 4, 2337)
;

INSERT INTO Matchups
	(Id, WinnerId, MatchupRound) VALUES
    (55, 2, 3),
    (78, 3, 1),
    (36, 5, 3),
    (5, 4, 8)
;

INSERT INTO MatchupEntries
	(Id, MatchupId, ParentMatchupId, TeamCompetingId, Score) VALUES
    (1, 55, 652, 2, 65),
    (4, 78, 23, 3, 80),
    (87, 36, 68, 5, 30),
    (51, 5, 789, 4, 72)
;

/*
	Works
*/

/*
	Some queries
*/

CALL MatchupEntriesIDInOrder







